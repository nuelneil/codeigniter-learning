<html>
	<head>
<?php 
	
	$this->load->helper('url'); 
	$this->load->library('javascript');
	$this->load->library('form_validation');
?>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dataTables.js" ></script>
	<style type="text/css" title="currentStyle" src="<?php echo base_url();?>assets/css/demo_table.css"></style>
	<style type="text/css" title="currentStyle" src="<?php echo base_url();?>assets/css/demo_table_jui.css"></style>

	<script language="javascript" type="text/javascript" >
		$(document).ready( function () {
    		$('#table_id').dataTable({
    			"sPaginationType": "full_numbers"
    		});
		} );


		function confirmGetMessage(id) {
		  	//display a confirmation box asking the visitor if they want to get a message
		  	var theAnswer = confirm("Do you really want to delete this member ?");
		  	//if the user presses the "OK" button display the message "Javascript is cool!!"
		  	if (theAnswer){
		  		alert("member is Deleted.");
		  		return true;
		  	}
		 	//otherwise display another message
		 	else{
		    	
		    	return false;
		  	}
		};

		function saveUpdate(){
			var theMessage = confirm("Do you really want to save the changes ?");
			if (theMessage){
				return true;
			} else {
				return false;
			}
		};
	</script>
	</head>
	<body>
<?php
	
	echo "<h1>Master Member Application</h1>";
	echo "<hr>";
?>