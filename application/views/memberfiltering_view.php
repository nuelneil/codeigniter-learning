		<?php
			function negate($by) {
				if (isset($_GET["sortby"]) && isset($_GET["type"])) {
					if ($_GET["sortby"] == $by) {
						if ($_GET["type"] == "asc") {
							return "desc";
						}
						else {
							return "asc";
						}
					}
				}
				
				return "asc";
			}

			function page(){
				if (isset($_GET["currentpage"])) {
					return $_GET["currentpage"];
				}
				return 1;
			}

			function sortby() {
				if (isset($_GET["sortby"])) {
					return $_GET["sortby"];
				}
				return "id";
			}

			function type_sort() {
				if(isset($_GET["type"])) {
					return $_GET["type"];
				}
				return "asc";
			}
		?>

		<div class="filtering">
			<?php $this->load->helper('form'); ?>
			<?php echo form_open("member/filter", array('method' => 'get')); ?>
			<h5>Find your data Here! :</h5>
			<table>
				<tr>
					<td>Firstname</td>
					<td>:</td>
					<td><input type="text" name="filterfirstname" id="filterfirstname" value="<?php echo $_GET['filterfirstname']; ?>" size="15"/></td>
					<td>Lastname</td>
					<td>:</td>
					<td><input type="text" name="filterlastname" id="filterlastname" value="<?php echo  $_GET['filterlastname']; ?>" size="15"/></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>:</td>
					<td>
			        	<?php 
			        		$educationOptions = array(
			        			'All', 'Male', 'Female'
		        			);

		        			foreach ($educationOptions as $option) {
		        		?>
							<input type="radio" name="filtergender" value="<?php echo $option ?>" <?php if (strcasecmp($option,  $_GET['filtergender']) == 0) echo "checked" ?>><?php echo $option ?>
		        		<?php
		        			}
			        	?>
					</td>
					<td>Email</td>
					<td>:</td>
					<td><input type="text" name="filteremail" id="filteremail" value="<?php echo $_GET['filteremail']; ?>" size="15"/></td>
				</tr>

				<tr></tr>
				<tr>
					<td><input type="submit" value="Search"></td>
	
				</tr>
			</table>
		</div>
		
		<br><br>
		<div class="menu">
			Create new member,<a href="<?php echo site_url('member/input');?>" >click here..</a>
		</div>

		<table id="myTable" class="tablesorter" border="2">
			<thead>
				<tr>
					<th>No</th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=firstname&type='. negate("firstname")); ?>" >Name</a></th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=gender&type='. negate("gender")); ?>" >Sex</a></th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=email&type='. negate("email")); ?>" >Email</a></th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=phone&type='. negate("phone")); ?>" >Phone</a></th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=education&type='. negate("education")); ?>" >Education</a></th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=address&type='. negate("address")); ?>" >Address</a></th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=facebook&type='. negate("facebook")); ?>" >Facebook</a></th>
					<th><a style="display: block; width: 100%;" href="<?php echo site_url('member/filter?currentpage='.page().'&filterfirstname='.$_GET['filterfirstname'].'&filterlastname='.$_GET['filterlastname'].'&filtergender='.$_GET['filtergender'].'&filteremail='.$_GET['filteremail'].'&sortby=twitter&type='. negate("twitter")); ?>" >Twitter</a></th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				$no = 1;
				foreach($query as $row){ 
			?>
			
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $row['firstname']." ".$row['lastname']; ?></td>
					<td><?php echo $row['gender']; ?></td>
					<td><?php echo $row['email']; ?></td>
					<td><?php echo $row['phone']; ?></td>
					<td><?php echo $row['education']; ?></td>
					<td><?php echo $row['address']; ?></td>
					<td><?php echo $row['facebook']; ?></td>
					<td>@<?php echo $row['twitter']; ?></td>
						<?php $edit_member = array('member','edit', $row['id']); ?>
					<td><a href="<?php echo site_url($edit_member); ?>"><input type="button" value="Edit" /></a></td>
						<?php $delete_member = array('member','del', $row['id']); ?>
					<td><a href="<?php echo site_url('member/del/' . $row['id']) ?>" onclick="return confirmGetMessage(<?php echo $row['id']; ?>);"><input type="button" value="Delete"></a></td>
				</tr>				
			
			<?php  $no++; }   ?>
			</tbody>
		</table>
		<?php 
			$range = 1;

			if ($currentpage > 1) {
			   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1&filterfirstname=".$_GET["filterfirstname"]."&filterlastname=".$_GET["filterlastname"]."&filtergender=".$_GET["filtergender"]."&filteremail=".$_GET["filteremail"]."&sortby=".sortby()."&type=".type_sort()."'>First</a> ";
			   $prevpage = $currentpage - 1;
			   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage&filterfirstname=".$_GET["filterfirstname"]."&filterlastname=".$_GET["filterlastname"]."&filtergender=".$_GET["filtergender"]."&filteremail=".$_GET["filteremail"]."&sortby=".sortby()."&type=".type_sort()."'><</a> ";
			}

			for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
			   if (($x > 0) && ($x <= $totalpages)) {
			      if ($x == $currentpage) {
			         echo " [<b>$x</b>] ";
			      } else {
			         echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x&filterfirstname=".$_GET["filterfirstname"]."&filterlastname=".$_GET["filterlastname"]."&filtergender=".$_GET["filtergender"]."&filteremail=".$_GET["filteremail"]."&sortby=".sortby()."&type=".type_sort()."'>$x</a> ";
			      }
			   }
			}

			if ($currentpage != $totalpages) {
			   $nextpage = $currentpage + 1; 
			   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage&filterfirstname=".$_GET["filterfirstname"]."&filterlastname=".$_GET["filterlastname"]."&filtergender=".$_GET["filtergender"]."&filteremail=".$_GET["filteremail"]."&sortby=".sortby()."&type=".type_sort()."'>></a> ";
			   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages&filterfirstname=".$_GET["filterfirstname"]."&filterlastname=".$_GET["filterlastname"]."&filtergender=".$_GET["filtergender"]."&filteremail=".$_GET["filteremail"]."&sortby=".sortby()."&type=".type_sort()."'>Last</a> ";
			} // end if


		?>
	</body>
</html>