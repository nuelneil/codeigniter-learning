		<div class="filtering">
			<?php $this->load->helper('form'); ?>
			<?php echo form_open("member/filter", array('method' => 'get')); ?>
			<h5>Find your data Here! :</h5>
			<table>
				<tr>
					<td>Firstname</td>
					<td>:</td>
					<td><input type="text" name="filterfirstname" id="filterfirstname" value="" size="15"/></td>
					<td>Lastname</td>
					<td>:</td>
					<td><input type="text" name="filterlastname" id="filterlastname" value="" size="15"/></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>:</td>
					<td>
						<input type="radio" name="filtergender" value="All" checked>All
						<input type="radio" name="filtergender" value="Male">Male
						<input type="radio" name="filtergender" value="Female">Female
					</td>
					<td>Email</td>
					<td>:</td>
					<td><input type="text" name="filteremail" id="filteremail" value="" size="15"/></td>
				</tr>

				<tr></tr>
				<tr>
					<td><input type="submit" value="Search"></td>
	
				</tr>
			</table>
		</div>
		
		<br><br>
		<div class="menu">
			Create new member,<a href="<?php echo site_url('member/input');?>" >click here..</a>
		</div>

		<table id="myTable" class="tablesorter" border="2">
			<thead>
				<tr>
					<!-- <th>No</th> -->
					<th>Name</th>
					<th>Sex</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Education</th>
					<th>Address</th>
					<th>Facebook</th>
					<th>Twitter</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				// $no = 1;
				foreach($results as $data){ 
			?>
			
				<tr>
					<!-- <td><?php echo $no; ?></td> -->
					<td><?php echo $data['firstname']." ".$data['lastname']; ?></td>
					<td><?php echo $data['gender']; ?></td>
					<td><?php echo $data['email']; ?></td>
					<td><?php echo $data['phone']; ?></td>
					<td><?php echo $data['education']; ?></td>
					<td><?php echo $data['address']; ?></td>
					<td><?php echo $data['facebook']; ?></td>
					<td>@<?php echo $data['twitter']; ?></td>
						<?php $edit_member = array('member','edit', $data['id']); ?>
					<td><a href="<?php echo site_url($edit_member); ?>"><input type="button" value="Edit" /></a></td>
						<?php $delete_member = array('member','del', $data['id']); ?>
					<td><a href="<?php echo site_url('member/del/' . $data['id']) ?>" onclick="return confirmGetMessage(<?php echo $data['id']; ?>);"><input type="button" value="Delete"></a></td>
				</tr>				
			
			<?php  }   ?>
			</tbody>
		</table>

		   <p><?php echo $links; ?></p>

		<script>
		$(document).ready(function() 
		    { 
		        $("#myTable")

		        .tablesorter({
		        	sortList: [[0,0], [1,0]],
		        	widthFixed: true, 
		        	widgets: ['zebra'],
		        	
		        });
		    } 
		);
		</script>
	</body>
</html>