		<div class="filtering">
			<?php $this->load->helper('form'); ?>
			<?php echo form_open("member/filter", array('method' => 'get')); ?>
			<h5>Find your data Here! :</h5>
			<table>
				<tr>
					<td>Firstname</td>
					<td>:</td>
					<td><input type="text" name="filterfirstname" id="filterfirstname" value="<?php echo $_GET['filterfirstname']; ?>" size="15"/></td>
					<td>Lastname</td>
					<td>:</td>
					<td><input type="text" name="filterlastname" id="filterlastname" value="<?php echo  $_GET['filterlastname']; ?>" size="15"/></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>:</td>
					<td>
			        	<?php 
			        		$educationOptions = array(
			        			'All', 'Male', 'Female'
		        			);

		        			foreach ($educationOptions as $option) {
		        		?>
							<input type="radio" name="filtergender" value="<?php echo $option ?>" <?php if (strcasecmp($option,  $_GET['filtergender']) == 0) echo "checked" ?>><?php echo $option ?>
		        		<?php
		        			}
			        	?>
					</td>
					<td>Email</td>
					<td>:</td>
					<td><input type="text" name="filteremail" id="filteremail" value="<?php echo $_GET['filteremail']; ?>" size="15"/></td>
				</tr>

				<tr></tr>
				<tr>
					<td><input type="submit" value="Search"></td>
	
				</tr>
			</table>
		</div>
		
		<br><br>
		<div class="menu">
			Create new member,<a href="<?php echo site_url('member/input');?>" >click here..</a>
		</div>

		<table id="myTable" class="tablesorter" border="2">
			<thead>
				<tr>
					<!-- <th>No</th> -->
					<th>Name</th>
					<th>Sex</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Education</th>
					<th>Address</th>
					<th>Facebook</th>
					<th>Twitter</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				// $no = 1;
				foreach($query as $row){ 
			?>
			
				<tr>
					<!-- <td><?php echo $no; ?></td> -->
					<td><?php echo $row['firstname']." ".$row['lastname']; ?></td>
					<td><?php echo $row['gender']; ?></td>
					<td><?php echo $row['email']; ?></td>
					<td><?php echo $row['phone']; ?></td>
					<td><?php echo $row['education']; ?></td>
					<td><?php echo $row['address']; ?></td>
					<td><?php echo $row['facebook']; ?></td>
					<td>@<?php echo $row['twitter']; ?></td>
						<?php $edit_member = array('member','edit', $row['id']); ?>
					<td><a href="<?php echo site_url($edit_member); ?>"><input type="button" value="Edit" /></a></td>
						<?php $delete_member = array('member','del', $row['id']); ?>
					<td><a href="<?php echo site_url('member/del/' . $row['id']) ?>" onclick="return confirmGetMessage(<?php echo $row['id']; ?>);"><input type="button" value="Delete"></a></td>
				</tr>				
			
			<?php  }   ?>
			</tbody>
		</table>

		<script>
		$(document).ready(function() 
		    { 
		        $("#myTable")

		        .tablesorter({
		        	sortList: [[0,0], [1,0]],
		        	widthFixed: true, 
		        	widgets: ['zebra'],
		        	
		        });
		    } 
		);
		</script>
	</body>
</html>