		<table id="table_id" class="display" border="1" >
			<thead>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Sex</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Education</th>
					<th>Address</th>
					<th>Facebook</th>
					<th>Twitter</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				$no = 1;
				foreach($query as $row){ 
			?>
			
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $row['firstname']." ".$row['lastname']; ?></td>
					<td><?php echo $row['gender']; ?></td>
					<td><?php echo $row['email']; ?></td>
					<td><?php echo $row['phone']; ?></td>
					<td><?php echo $row['education']; ?></td>
					<td><?php echo $row['address']; ?></td>
					<td><?php echo $row['facebook']; ?></td>
					<td>@<?php echo $row['twitter']; ?></td>
						<?php $edit_member = array('member','edit', $row['id']); ?>
					<td><a href="<?php echo site_url($edit_member); ?>"><input type="button" value="Edit" /></a></td>
						<?php $delete_member = array('member','del', $row['id']); ?>
					<td><a href="<?php echo site_url('member/del/' . $row['id']) ?>" onclick="return confirmGetMessage(<?php echo $row['id']; ?>);"><input type="button" value="Delete"></a></td>
				</tr>				
			
			<?php  $no++; }   ?>
			</tbody>
		</table>
	</body>
</html>