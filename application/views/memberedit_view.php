<html>
	<body>

		<?php 
			$this->load->helper('form');
			echo form_open('member/entry_update');
		?>
		<table>
			<input type="hidden" name="id" id="id" value="<?php echo $query['id'];?>" />
		    <tr>
		       	<td><label for="firstname">First Name</label></td>
                <td>:</td>
                <td><input type="text" name="firstName" id="firstName" value="<?php echo $query['firstname'];  ?>" size="30"/></td>
		    	<td><?php echo form_error('firstName'); ?></td>
		    </tr>
		     <tr>
		        <td><label for="lastName">Last Name</label></td>
		        <td>:</td>
		        <td><input type="text" name="lastName" id="lastName" value="<?php echo $query['lastname'];  ?>" size="30"/></td>
		    	<td><?php echo form_error('lastName'); ?></td>
		    </tr>
		    <tr>
		        <td><label for="sex">Sex</label></td>
		        <td>:</td>
		        <td>
		        	<?php if($query['gender'] == 'Male') { ?>
		            	<input type="radio" name="sex" value="Male" checked>Male<br>
		            	<input type="radio" name="sex" value="Female">Female
		            <?php } else { ?>
		            	<input type="radio" name="sex" value="Male">Male<br>
		            	<input type="radio" name="sex" value="Female" checked>Female
		            <?php } ?>
		        </td>
		    </tr>
		    <tr>
		        <td><label for="email">Email</label></td>
		        <td>:</td>
		        <td><input type="text" name="email" id="email" value="<?php echo $query['email'];  ?>" size="30"/></td>
		    	<td><?php echo form_error('email'); ?></td>
		    </tr>
		    <tr>
		        <td><label for="phone">Phone</label></td>
		        <td>:</td>
		        <td><input type="text" name="phone" id="phone" value="<?php echo $query['phone'];  ?>" size="30"/></td>
		    	 <td><?php echo form_error('phone'); ?></td>
		    </tr>
		    <tr>
		        <td><label for="education">Education</label></td>
		        <td>:</td>
		        <td>
		        	<select id="edu" name="education">
			        	<?php 
			        		$educationOptions = array(
			        			'Diploma', 'Bachelor', 'Master', 'Professor'
		        			);

		        			foreach ($educationOptions as $option) {
		        		?>
							<option value="<?php echo $option ?>" <?php if (strcasecmp($option, $query['education']) == 0) echo "selected" ?>><?php echo $option ?></option>
		        		<?php
		        			}
			        	?>
		            </select>
		        </td>
		    </tr>
		    <tr>
		        <td><label for="address">Address</label></td>
		        <td>:</td>
		        <td><textarea type="text" name="address" id="address" value="" cols="24" rows="4"><?php echo $query['address'];  ?></textarea></td>
		    	<td><?php echo form_error('address'); ?></td>
		    </tr>
		     <tr>
		        <td><label for="facebook">Facebook</label></td>
		        <td>:</td>
		        <td><input type="text" name="facebook" id="facebook" value="<?php echo $query['facebook'];  ?>" size="30"/></td>
		    	 <td><?php echo form_error('facebook'); ?></td>
		    </tr>
		    <tr>
		        <td><label for="twitter">Twitter</label></td>
		        <td>:</td>
		        <td><input type="text" name="twitter" id="twitter" value="<?php echo $query['twitter'];  ?>" size="30"/></td>
		   		<td><?php echo form_error('twitter'); ?></td>
		    </tr>
		    
		    <tr>
		        <!-- <td><input type="submit" value="Submit" name="mysubmit" /></td> -->
		        <td><a href="<?php echo site_url('member/') ?>" onclick="return saveUpdate();"><input type="submit" value="Submit" name="mysubmit"></a></td>
		    </tr>       
		            
		</table>

	</body>
</html>