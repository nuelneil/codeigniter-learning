<div id="content">
    <?php $this->load->helper('form'); ?>
    <?php echo form_open("member/insert_data"); ?>
        <table>

            <tr>
                <td><label for="firstName">First Name</label></td>
                <td>:</td>
                <td><input type="text" name="firstName" id="firstName" value="<?php echo set_value('firstName'); ?>" size="30"/></td>
                <td><?php echo form_error('firstName'); ?></td>
            </tr>
             <tr>
                <td><label for="lastName">Last Name</label></td>
                <td>:</td>
                <td><input type="text" name="lastName" id="lastName" value="<?php echo set_value('lastName'); ?>" size="30" /></td>
                <td><?php echo form_error('lastName'); ?></td>
            </tr>
            <tr>
                <td><label for="sex">Sex</label></td>
                <td>:</td>
                <td>
                    <input type="radio" name="sex" value="Male" checked>Male<br>
                    <input type="radio" name="sex" value="Female">Female
                </td>
            </tr>
            <tr>
                <td><label for="email">Email</label></td>
                <td>:</td>
                <td><input type="text" name="email" id="email" value="<?php echo set_value('email'); ?>" size="30"/></td>
                <td><?php echo form_error('email'); ?></td>
            </tr>
            <tr>
                <td><label for="phone">Phone</label></td>
                <td>:</td>
                <td><input type="text" name="phone" id="phone" value="<?php echo set_value('phone'); ?>" size="30"/></td>
                <td><?php echo form_error('phone'); ?></td>
            </tr>
            <tr>
                <td><label for="education">Education</label></td>
                <td>:</td>
                <td>
                    <select name="education">
                      <option value="Diploma">Diploma</option>
                      <option value="Bachelor">Bachelor</option>
                      <option value="Master">Master</option>
                      <option value="Professor">Profesor</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="address">Address</label></td>
                <td>:</td>
                <td><textarea type="text" name="address" id="address" cols="24" rows="4"><?php echo set_value('address'); ?></textarea></td>
                <td><?php echo form_error('address'); ?></td>
            </tr>
            <tr>
                <td><label for="facebook">Facebook</label></td>
                <td>:</td>
                <td><input type="text" name="facebook" id="facebook" value="<?php echo set_value('facebook'); ?>" size="30"/></td>
                <td><?php echo form_error('facebook'); ?></td>
            </tr>
            <tr>
                <td><label for="twitter">Twitter</label></td>
                <td>:</td>
                <td><input type="text" name="twitter" id="twitter" value="<?php echo set_value('twitter'); ?>" size="30"/></td>
                <td><?php echo form_error('twitter'); ?></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit" name="mysubmit" /></td>
            </tr>       
                    
        </table>
    <?php echo form_close(); ?>
</div><!--<div id="content">-->