<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Datatable_model extends CI_Model {

        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        public function get_all()
        {
            $query=$this->db->get('member');
            return $query->result_array();
        }

        public function get($id)
        {
            $this->load->database();
            $query = $this->db->get_where('member',array('id'=>$id));
            return $query->row_array();        
        }

        public function entry_update($data_update)
        {

            $id = $this->input->post('id');
            $this->db->where('id', $id);
            $this->db->update('member', $data_update);
             
        }

        public function delete($id)
        {
           $this->load->database();
           $this->db->delete('member', array('id' => $id)); 
        }

    }
?>