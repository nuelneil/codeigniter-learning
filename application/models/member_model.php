<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Member_model extends CI_Model {

        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        public function add_member()
        {
            $data_member=array(
                'firstname'=>$_POST['firstName'],
                'lastname'=>$_POST['lastName'],
                'gender'=>$_POST['sex'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'education'=>$_POST['education'],
                'address'=>$_POST['address'],
                'facebook'=>$_POST['facebook'],
                'twitter'=>$_POST['twitter'],
                );
            $this->db->insert('member',$data_member);
        }

        public function get_all()
        {
            $query=$this->db->get('member');
            return $query->result_array();
        }

        public function get($id)
        {
            $this->load->database();
            $query = $this->db->get_where('member',array('id'=>$id));
            return $query->row_array();        
        }

        public function entry_update($data_update)
        {

            $id = $this->input->post('id');
            $this->db->where('id', $id);
            $this->db->update('member', $data_update);
             
        }

        public function delete($id)
        {
           $this->load->database();
           $this->db->delete('member', array('id' => $id)); 
        }

        public function filter($firstname, $lastname, $gender, $email, $offset, $per_page)
        {
            if (isset($_GET["sortby"]) && isset($_GET["type"])) {
                $by = $_GET['sortby'];
                $type = $_GET['type'];
                $this->db->order_by($by,$type);
            }  

            if ($gender != "All") {
                $this->db->where('gender',$gender);
            } else {
                $this->db->where('gender !=','Null');
            } 
            $this->db->like('firstname',$firstname);
            $this->db->like('lastname',$lastname);
            $this->db->like('email',$email);

            $query = $this->db->get('member', $per_page, $offset);
            $rowfilter = $query->num_rows();
            return $query->result_array();
        }

        //Script for Sorting
        // public function order($sorting, $type)
        // {
        //     $this->db->order_by($sorting,$type);
        //     $query=$this->db->get('member');
        //     return $query->result_array();  
        // }

        public function record_count()
        {
            return $this->db->count_all("member");
        }

        public function pagination($offset, $per_page)
        {
            if (isset($_GET["sortby"]) && isset($_GET["type"])) {
                $by = $_GET['sortby'];
                $type = $_GET['type'];
                $this->db->order_by($by,$type);
            } 

            $query = $this->db->get('member', $per_page, $offset);
            return $query->result_array();

        }
    }
?>