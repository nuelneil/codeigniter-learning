<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __costruct()
  {
		parent::__construct();
    $this->load->helper("url");
    $this->load->model("member_model");
    $this->load->library("pagination");
	}

	// public function index()
	// {
 //    $this->load->model("member_model");
 //    $this->load->library("pagination");
 //    $this->load->view('header');

 //    $data["query"] = $this->member_model->get_all();
 //    $this->load->view("memberlist_view", $data);

	// }

	public function input() 
	{
		//function for input page
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->helper('url');
    $this->load->model('member_model');
		$this->load->view('header');
    $this->load->view('memberinput_view');  
	}

	public function insert_data()
  {
	  //script for validation 
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');

    $this->form_validation->set_rules('firstName', 'Firstname', 'required');
    $this->form_validation->set_rules('lastName', 'Lastname', 'required');
    $this->form_validation->set_rules('sex', 'Sex', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[member.email]');
    $this->form_validation->set_rules('phone', 'Phone Number', 'required');
    $this->form_validation->set_rules('education', 'Education', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');
    $this->form_validation->set_rules('facebook', 'Facebook', 'required');
    $this->form_validation->set_rules('twitter', 'Twitter', 'required');
    $data['message'] = 'not validated';
    if ($this->form_validation->run() == FALSE)
    {
      validation_errors();
      $this->load->view('header');
      $this->load->view('memberinput_view', $data);
    }
    else
    {
      //Script for Insert Database | only when validated will be inserted
      $this->load->model('member_model');
      $this->member_model->add_member();
      $this->load->helper('url');
      $id = mysql_insert_id();
      redirect('/member/success_inserted/'.$id);
    } 
  }

  public function success_inserted($id)
  {
    $this->load->helper('form');
    $this->load->helper('html');
    $this->load->helper('url');
    $this->load->model('member_model');
    $this->load->view('header');
    $this->load->view('success_inserted');

    // script take the member data base on id member
    $data['query'] = $this->member_model->get($id);

    $this->load->view('memberedit_view', $data);
  }

  public function edit($id)
  {
   	$this->load->helper('form');
   	$this->load->helper('html');
   	$this->load->helper('url');
  	$this->load->model('member_model');
   	$this->load->view('header');

   	// script take the member data base on id member
   	$data['query'] = $this->member_model->get($id);

  	$this->load->view('memberedit_view', $data); 
	}

	public function entry_update()
  {
  	//script for update to database
    	$this->load->helper('form');
      $this->load->helper('html');
      $this->load->helper('url');

    // Script Validation
      $this->load->helper('url');
      $this->load->database();
      $this->load->library('form_validation');

      $this->form_validation->set_rules('firstName', 'Firstname', 'required');
      $this->form_validation->set_rules('lastName', 'Lastname', 'required');
      $this->form_validation->set_rules('sex', 'Sex', 'required');
      $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
      $this->form_validation->set_rules('phone', 'Phone Number', 'required');
      $this->form_validation->set_rules('education', 'Education', 'required');
      $this->form_validation->set_rules('address', 'Address', 'required');
      $this->form_validation->set_rules('facebook', 'Facebook', 'required');
      $this->form_validation->set_rules('twitter', 'Twitter', 'required');
      $data['message'] = 'not validated';
      if ($this->form_validation->run() == FALSE)
      {
        validation_errors();
        $this->load->view('header');
        $this->load->view('memberinput_view');
      }
      else
      {
        //Script for Insert Database | only when validated will be inserted
        $data_update = array(
                      'firstname'=>$this->input->post('firstName'),
                      'lastname'=>$this->input->post('lastName'),
                      'gender'=>$this->input->post('sex'),
                      'email'=>$this->input->post('email'),
                      'phone'=>$this->input->post('phone'),
                      'education'=>$this->input->post('education'),
                      'address'=>$this->input->post('address'),
                      'facebook'=>$this->input->post('facebook'),
                      'twitter'=>$this->input->post('twitter'),
              );
            
              $this->load->model('member_model');
              $this->member_model->entry_update($data_update);

              $this->load->helper('url');
              redirect('/');
      }   
  }

  public function del($id)
  {
    $this->load->library('table');
  	$this->load->helper('html');	
  	$this->load->model('member_model');

  	if((int)$id > 0){
   	  	$this->member_model->delete($id);
  	}

    $this->load->helper('url');
    redirect('/');

  }

  // public function order()
  // {
  //   $this->load->database();
  //   $this->load->model('member_model');

  //   $sorting = $_GET['by'];
  //   $type = $_GET['type'];
  //   $data['query'] = $this->member_model->order($sorting, $type);
  //   $this->load->view('header');
  //   $this->load->view('memberlist_view', $data);
  // }

  public function filter()
  {
    $this->load->helper('form');
    $this->load->helper('html');
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('member_model');

    $firstname = $_GET['filterfirstname'];
    $lastname = $_GET['filterlastname'];
    $gender = $_GET['filtergender'];
    $email = $_GET['filteremail'];

    //combine to pagination
        $data['total_row']  = $this->member_model->record_count();
        $data['per_page']   = 10;
        $data['totalpages'] = ceil(  $data['total_row'] /   $data['per_page']);

        if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
          $data['currentpage'] = (int) $_GET['currentpage'];
        } else {
          $data['currentpage'] = 1;
        }

        if (  $data['currentpage'] >   $data['totalpages']) {
          $data['currentpage'] =   $data['totalpages'];
        }

        if (  $data['currentpage'] < 1) {
          $data['currentpage'] = 1;
        }

        $data['offset'] = (  $data['currentpage'] - 1) *   $data['per_page'];
    //end of pagination

    //combine to order
        if (isset($_GET["sortby"]) && isset($_GET["type"])) {
          $data['sortby'] = $_GET['sortby'];
          $data['type'] = $_GET['type'];
          $data['query'] = $this->member_model->filter($firstname, $lastname, $gender, $email, $data['offset'], $data['per_page'], $data['sortby'], $data['type']);
        } 
    //end of combining order

    $data['query'] = $this->member_model->filter($firstname, $lastname, $gender, $email,  $data['offset'], $data['per_page']);
   
    $this->load->view('header');
    $this->load->view('memberfiltering_view',$data);
  }

  public function index() 
  {
    $this->load->helper('form');
    $this->load->helper('html');
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('member_model');

    $data['total_row']  = $this->member_model->record_count();
    $data['per_page']   = 5;
    $data['totalpages'] = ceil(  $data['total_row'] /   $data['per_page']);

    if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
      $data['currentpage'] = (int) $_GET['currentpage'];
    } else {
      $data['currentpage'] = 1;
    }

    if (  $data['currentpage'] >   $data['totalpages']) {
      $data['currentpage'] =   $data['totalpages'];
    }

    if (  $data['currentpage'] < 1) {
      $data['currentpage'] = 1;
    }

    $data['offset'] = (  $data['currentpage'] - 1) *   $data['per_page'];

    //combine to order
    if (isset($_GET["sortby"]) && isset($_GET["type"])) {
      $data['sortby'] = $_GET['sortby'];
      $data['type'] = $_GET['type'];
      $data['query'] = $this->member_model->pagination($data['offset'], $data['per_page'], $data['sortby'], $data['type']);
    } 
    // end of combine to order

    $data["query"] = $this->member_model->pagination(  $data['offset'], $data['per_page']);
    $this->load->view('header');
    $this->load->view('memberlist_view', $data);

  }
}
?>

