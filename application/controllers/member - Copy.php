<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

  // print '<pre>';
  // print_r($data_update);
  // die();

	public function __costruct(){
		parent::__construct();
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('header');
    $this->load->library('pagination');

		//Script for grabbing Database
		$this->load->model("member_model");
		$data['query']=$this->member_model->get_all();
		$this->load->view('memberlist_view',$data);
	}

	public function input() 
	{
		//function for input page
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->helper('url');
    $this->load->model('member_model');
		$this->load->view('header');
    $this->load->view('memberinput_view');  
	}

	public function insert_data()
  {
	  //script for validation 
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');

    $this->form_validation->set_rules('firstName', 'Firstname', 'required');
    $this->form_validation->set_rules('lastName', 'Lastname', 'required');
    $this->form_validation->set_rules('sex', 'Sex', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[member.email]');
    $this->form_validation->set_rules('phone', 'Phone Number', 'required');
    $this->form_validation->set_rules('education', 'Education', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');
    $this->form_validation->set_rules('facebook', 'Facebook', 'required');
    $this->form_validation->set_rules('twitter', 'Twitter', 'required');
    $data['message'] = 'not validated';
    if ($this->form_validation->run() == FALSE)
    {
      validation_errors();
      $this->load->view('header');
      $this->load->view('memberinput_view', $data);
    }
    else
    {
      //Script for Insert Database | only when validated will be inserted
      $this->load->model('member_model');
      $this->member_model->add_member();
      $this->load->helper('url');
      $id = mysql_insert_id();
      redirect('/member/success_inserted/'.$id);
    } 
  }

  public function success_inserted($id)
  {
    $this->load->helper('form');
    $this->load->helper('html');
    $this->load->helper('url');
    $this->load->model('member_model');
    $this->load->view('header');
    $this->load->view('success_inserted');

    // script take the member data base on id member
    $data['query'] = $this->member_model->get($id);

    $this->load->view('memberedit_view', $data);
  }

  public function edit($id)
  {
   	$this->load->helper('form');
   	$this->load->helper('html');
   	$this->load->helper('url');
  	$this->load->model('member_model');
   	$this->load->view('header');

   	// script take the member data base on id member
   	$data['query'] = $this->member_model->get($id);

  	$this->load->view('memberedit_view', $data); 
	}

	public function entry_update()
  {
  	//script for update to database
    	$this->load->helper('form');
      $this->load->helper('html');
      $this->load->helper('url');

    // Script Validation
      $this->load->helper('url');
      $this->load->database();
      $this->load->library('form_validation');

      $this->form_validation->set_rules('firstName', 'Firstname', 'required');
      $this->form_validation->set_rules('lastName', 'Lastname', 'required');
      $this->form_validation->set_rules('sex', 'Sex', 'required');
      $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
      $this->form_validation->set_rules('phone', 'Phone Number', 'required');
      $this->form_validation->set_rules('education', 'Education', 'required');
      $this->form_validation->set_rules('address', 'Address', 'required');
      $this->form_validation->set_rules('facebook', 'Facebook', 'required');
      $this->form_validation->set_rules('twitter', 'Twitter', 'required');
      $data['message'] = 'not validated';
      if ($this->form_validation->run() == FALSE)
      {
        validation_errors();
        $this->load->view('header');
        $this->load->view('memberinput_view');
      }
      else
      {
        //Script for Insert Database | only when validated will be inserted
        $data_update = array(
                      'firstname'=>$this->input->post('firstName'),
                      'lastname'=>$this->input->post('lastName'),
                      'gender'=>$this->input->post('sex'),
                      'email'=>$this->input->post('email'),
                      'phone'=>$this->input->post('phone'),
                      'education'=>$this->input->post('education'),
                      'address'=>$this->input->post('address'),
                      'facebook'=>$this->input->post('facebook'),
                      'twitter'=>$this->input->post('twitter'),
              );
            
              $this->load->model('member_model');
              $this->member_model->entry_update($data_update);

              $this->load->helper('url');
              redirect('/');
      }   
  }

  public function del($id)
  {
    $this->load->library('table');
  	$this->load->helper('html');	
  	$this->load->model('member_model');

  	if((int)$id > 0){
   	  	$this->member_model->delete($id);
  	}

    $this->load->helper('url');
    redirect('/');

  }

  public function filter()
  {
      // print '<pre>';
      // print_r($_GET);
      // die();

      $this->load->helper('form');
      $this->load->helper('html');
      $this->load->helper('url');
      $this->load->database();

      $firstname = $_GET['filterfirstname'];
      $lastname = $_GET['filterlastname'];
      $gender = $_GET['filtergender'];
      $email = $_GET['filteremail'];

      $this->load->model('member_model');
      $data['query']=$this->member_model->filter($firstname, $lastname, $gender, $email);
      $this->load->view('header');
      $this->load->view('memberfiltering_view',$data);
  }

  public function example1() {
          $config = array();
          $config["base_url"] = base_url() . "welcome/example1";
          $config["total_rows"] = $this->Countries->record_count();
          $config["per_page"] = 20;
          $config["uri_segment"] = 3;

          $this->pagination->initialize($config);

          $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
          $data["results"] = $this->Countries->
              fetch_countries($config["per_page"], $page);
          $data["links"] = $this->pagination->create_links();

          $this->load->view("example1", $data);
      }
}
?>