<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Datatable extends CI_Controller {

    public function __costruct()
    {
    	parent::__construct();
      $this->load->helper('url');
      $this->load->model('datatable_model');
    }

    public function index()
    {
      $this->load->model('datatable_model');
      $this->load->view('header');

      $data['query'] = $this->datatable_model->get_all();
      $this->load->view('datatablelist_view', $data);
    }
  }
?>

