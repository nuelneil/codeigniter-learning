/*1368455326,178130467*/

if (self.CavalryLogger) { CavalryLogger.start_js(["5XXNg"]); }

__d("HomeContextSaveItem",["Arbiter","copyProperties","Class","CSS","MenuItem"],function(a,b,c,d,e,f){var g=b('Arbiter'),h=b('copyProperties'),i=b('Class'),j=b('CSS'),k=b('MenuItem');function l(m){this.parent.construct(this,m);g.subscribe('savedstorychanged/'+m.name,this._change.bind(this));}i.extend(l,k);l.informChanged=function(m,n){g.inform('savedstorychanged/'+m,n);};h(l.prototype,{_change:function(m,n){if(n.uri)this._anchor.setAttribute('ajaxify',n.uri);if(n.visible!==undefined)j.conditionShow(this._root,n.visible);}});e.exports=l;});
__d("legacy:highlight",["highlight"],function(a,b,c,d){a.highlight=b('highlight');},3);