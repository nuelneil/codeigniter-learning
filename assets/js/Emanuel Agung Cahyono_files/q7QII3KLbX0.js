/*1372045832,178146657*/

if (self.CavalryLogger) { CavalryLogger.start_js(["NRHQk"]); }

__d("OGComposerTypeaheadMetrics",["TypeaheadMetrics"],function(a,b,c,d,e,f){var g=b('TypeaheadMetrics'),h=g,i=h&&h.prototype?h.prototype:h;function j(){if(h&&h.prototype)h.apply(this,arguments);}for(var k in h)if(k!=="_metaprototype"&&h.hasOwnProperty(k))j[k]=h[k];j.prototype=Object.create(i);j.prototype.constructor=j;j.prototype.submit=function(){this.extraData.context='at_'+this.data.queryData.at_id;i.submit.call(this);};e.exports=j;});